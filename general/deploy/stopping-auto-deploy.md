# Overview

This page is dedicated to the process of [auto-deploy](auto-deploy.md).

## Temporarily stopping automated deployments

In case of an issue that could cause a severe disruption of GitLab.com and the related environments, it might be necessary to stop automatic deployments from
running.

In case of such an event:

* Issue describing P1 and S1 needs to be created.
* Project maintainer or owner of the [release-tools]() project needs to go to
[the pipeline schedule](https://gitlab.com/gitlab-org/release-tools/pipeline_schedules) and disable the `Auto-deploy tagging` job.
* When the job is disabled, inform the release managers of this fact through
the `#releases` channel

Stopping the `Auto-deploy tagging` job will effectively prevent the tooling from creating new packages and triggering automated deploys to the staging environment. Ensuring that release managers are informed will ensure that manual part of deployment (progression to production environments) is not executed.

Once the issue is communicated, release managers need to make a decision on case per case basis on what is the best way to progress further towards enabling the automated deployments again.
